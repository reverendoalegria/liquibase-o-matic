package dridco.liquibase

trait ChangelogObject {
  def schema: String
  def name: String 
  def code: String
  def dropIfExistsTemplate: (ChangelogObject) => String
}

trait SysChangelogObject extends ChangelogObject {
  def objectTypeCodes: Seq[String]
  def objectTypeName: String
  def dropIfExistsTemplate: (ChangelogObject) => String = changeLogObject =>
s"""
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[${schema}].[${name}]') AND type in (${objectTypeCodes.map("N'" + _ + "'").mkString(",")}))
DROP ${objectTypeName} [${schema}].[${name}]
"""  
}

case class ProcedureChangelogObject(schema: String, name: String, code: String) extends SysChangelogObject {
  def objectTypeCodes = Seq("P", "PC")
  def objectTypeName: String = "PROC"
} 

case class TriggerChangelogObject(schema: String, name: String, code: String) extends ChangelogObject {
  override def dropIfExistsTemplate: (ChangelogObject) => String = changeLogObject =>
s"""
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[${schema}].[${name}]'))
DROP TRIGGER [${schema}].[${name}]
"""
} 

case class FunctionChangelogObject(schema: String, name: String, code: String) extends SysChangelogObject {
  def objectTypeCodes = Seq("FN", "IF", "TF", "FS", "FT")
  def objectTypeName: String = "FUNCTION"  
} 

case class TypeChangelogObject(schema: String, name: String, code: String) extends ChangelogObject {
  override def dropIfExistsTemplate = changeLogObject => 
s"""
IF EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'${name}' AND ss.name = N'${schema}')
BEGIN
  DROP TYPE [${schema}].[${name}]
"""
}

case class RuleChangelogObject(schema: String, name: String, code: String) extends ChangelogObject {
    override def dropIfExistsTemplate = changeLogObject =>
s"""
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[${schema}].[${name}]') AND OBJECTPROPERTY(id, N'IsRule') = 1)
DROP RULE [${schema}].[${name}]
"""
}