package dridco.liquibase

import java.util.regex.Pattern

class TriggersChangelogHandler extends ChangelogHandler {
  def objectName = "Triggers"
  def countPattern = Pattern.compile("""CREATE[\s]+TRIGGER+[\s]+""", Pattern.DOTALL | Pattern.CASE_INSENSITIVE)
  def extractPattern = Pattern.compile("""CREATE[\s]+TRIGGER+[\s]+\[([\w\d]+)\]\.\[([\w\d_]+)\](.+?)GO$""", Pattern.DOTALL | Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE | Pattern.MULTILINE)
  def template = LiquibaseTemplates.sqlTemplate
  def changelogObject(schema: String, name: String, code: String) = new TriggerChangelogObject(schema, name, code)
}