package dridco.liquibase

import java.io.BufferedInputStream
import java.io.FileInputStream
import java.io.BufferedReader
import java.io.InputStreamReader
import java.nio.CharBuffer
import java.io.File
import java.io.FileWriter
import java.io.OutputStreamWriter
import java.io.FileOutputStream
import java.io.Writer
import java.io.ByteArrayInputStream
import java.io.PrintWriter
import org.w3c.dom.Document
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.parsers.DocumentBuilder
import java.io.InputStream
import scala.xml.XML
import java.io.FileReader
import org.w3c.dom.Node
import java.io.Reader
import org.w3c.dom.NodeList
import org.w3c.dom.Element
import java.io.OutputStream
import javax.xml.transform.TransformerFactory
import javax.xml.transform.dom.DOMSource
import javax.xml.transform.stream.StreamResult
import grizzled.slf4j.Logging

/**
 * <li>First run a query in MSSQL to obtain all default-constraints in a CSV format</li>
 * <li>Second run this program, using as parameters:
 *   <ol>1st: The CSV file with expected default-constraints</ol>
 *   <ol>2nd: The Liquibase XML that is missing default-constraints</ol>
 *   <ol>3rd: The directory to which you want the result to be written</ol>
 * </li> 
 */
object LiquiOMerge extends App {

    if (!args.isDefinedAt(2)) {
        println("Usage: LiquiOMerge <input-file> <merge-xml-file> <output-dir>")
        sys.exit
    }

    val content = LiquiOMatic.readFile(args(0))
    val mergeFile = new FileInputStream(args(1))
    val outputDir = new File(args(2))

    var mergeResultFile: OutputStream = _ 
    try {
    	mergeResultFile = new FileOutputStream(new File(outputDir, "merge-result.xml"))
    	val merger = new MergeLiquibaseDefaultConstraints(content, mergeFile)
    	merger.merge(mergeResultFile)
    } finally {
    	mergeResultFile.close()
    }
}

case class DefaultConstraint(table: String, column: String, definition: String)

class MergeLiquibaseDefaultConstraints(content: String, xml: InputStream) extends Logging {

    def constraints: Iterator[DefaultConstraint] = {
        content.lines.map { l =>
            val tokens = l.split("""\s*;\s*""")
            if (tokens.length != 3) throw new IllegalArgumentException(s"Invalid defaults CSV content. Expected [table, column, definition]. Got ${l}")
            DefaultConstraint(tokens(0), tokens(1), tokens(2))
        }
    }

    val constraintsTables: Set[String] = constraints.map ( dc => dc.table ) toSet

    val constraintsTableColumnMap: Map[String, DefaultConstraint] = constraints.map ( dc => ((dc.table + "." + dc.column) -> dc) ) toMap

    val doc: Document = {
		val factory: DocumentBuilderFactory = DocumentBuilderFactory.newInstance()
	    factory.setValidating(false)
	    factory.setIgnoringElementContentWhitespace(true)
        val builder: DocumentBuilder = factory.newDocumentBuilder()
        builder.parse(xml)
    }
    
    val columnsFilter: Node => Boolean = 
        n => n.getNodeType == Node.ELEMENT_NODE && n.getNodeName == "column"
        
    def merge(out: OutputStream): Unit = {
        import XMLImplicits._
        
    	info("Will process these constraints: " + constraints.mkString(", "))
        
        val tables = doc.getElementsByTagName("createTable")
        debug(s"Found ${tables.getLength} create-table elements")
        
        val changes = tables.foldLeft(Seq[DefaultConstraint]()) { (acc, t) =>
            val tableName = t.getAttributes.getNamedItem("tableName").getTextContent
            val changes = if (constraintsTables contains tableName) {
                debug(s"Navigating Table ${tableName}")
                t.getChildNodes.filter(columnsFilter).map( mergeColumn(tableName, _) ).flatten
            } else Seq()
            acc ++ changes
        }
        
        info(s"Expected ${constraints.length} default constraints")
        info(s"Affected ${changes.length} columns")
        
        val missing = constraintsTableColumnMap -- changes.map( dc => dc.table + "." + dc.column)
        warn(s"Missing ${missing.keys.mkString(", ")}!")
        
        val transf = TransformerFactory.newInstance.newTransformer()
        transf.transform(new DOMSource(doc), new StreamResult(out))
        
    }

    def mergeColumn(tableName: String, n: Node): Seq[DefaultConstraint] = {
        val columnName = n.getAttributes.getNamedItem("name").getTextContent
        val typeName = n.getAttributes.getNamedItem("type").getTextContent
        
        debug(s"Navigating Column ${columnName}")
        
        implicit class str2boolean(str: String) {
            def toNumericBoolean = if (str.toInt == 0) false else true
        }
        
        constraintsTableColumnMap.get(tableName + "." + columnName).map { dc =>
            
            val columnElem = n.asInstanceOf[Element]
            if (dc.definition contains "()") {
                // looks like a function to me...
                info(s"ADDING DEFAULT [${tableName}.${columnName}] => Function(${dc.definition})")
            	columnElem.setAttribute("defaultValueComputed", dc.definition)
            	
            } else {
                val trimmedDefinition = dc.definition.replaceAll("""[\(\)]*""", "")
                
                if (typeName.equalsIgnoreCase("BIT")) {
                    val booleanValue = trimmedDefinition.toNumericBoolean.toString
                    info(s"ADDING DEFAULT [${tableName}.${columnName}] => Boolean(${booleanValue})")
                	columnElem.setAttribute("defaultValueBoolean", booleanValue)
                } else {
	                // lets hope is a constant value
                    info(s"ADDING DEFAULT [${tableName}.${columnName}] => ${trimmedDefinition}")
	            	columnElem.setAttribute("defaultValue", trimmedDefinition)
                }
            }
            dc
        }.toSeq
    }
    
}

object XMLImplicits {
    implicit def nodeList2Iterator(nl: NodeList): Iterator[Node] = new Iterator[Node] {
        var c = 0
        def hasNext = c < nl.getLength
        def next() = {
            val n = nl.item(c)
            c += 1
            n
        }
    }
}