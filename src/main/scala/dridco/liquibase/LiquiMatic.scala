package dridco.liquibase

import java.io.BufferedInputStream
import java.io.FileInputStream
import java.io.BufferedReader
import java.io.InputStreamReader
import java.nio.CharBuffer
import java.util.regex.Pattern
import java.io.File
import java.io.FileWriter
import java.io.OutputStreamWriter
import java.io.FileOutputStream
import java.io.Writer
import scala.xml.Utility
import scala.io.Source
import scala.io.Codec

object LiquiOMatic extends App {

  if (!args.isDefinedAt(1)) {
    println("Usage: LiquiOMatic <input-file> <output-dir>")
    sys.exit
  }

  val content = readFile(args(0))
  val outputDir = new File(args(1))
  val handlers = Map(
      "procs" 		-> new ProceduresChangelogHandler(), 
      "triggers" 	-> new TriggersChangelogHandler(),
      "functions" 	-> new FunctionsChangelogHandler(),
      "types" 		-> new TypesChangelogHandler(),
      "rules" 		-> new RulesChangelogHandler()
  )
  
  handlers.seq.map {
    case (name, handler) => handler.export(content, new File(outputDir, name))
  }
  
  def readFile(path: String) = {
      Source.fromFile(path)(Codec("UTF-16LE")).mkString
      
//    val in = new BufferedReader(new InputStreamReader(new FileInputStream(path), "UTF-16LE"))
//    
//    val buff = CharBuffer.allocate(1024)
//
//    var c = in.read(buff)
//    val strBuilder = new StringBuilder(new String(buff.array, buff.arrayOffset, buff.limit))
//    while (c > 0) {
//      strBuilder.appendAll(buff.array, buff.arrayOffset, buff.limit)
//      buff.clear
//      c = in.read(buff)
//    }
//
//    strBuilder.toString
  }

}