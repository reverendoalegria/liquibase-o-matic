package dridco.liquibase

import java.io.Writer
import java.io.OutputStreamWriter
import java.util.regex.Pattern
import java.io.File
import java.io.FileOutputStream

trait ChangelogHandler {
  
  def objectName: String
  
  def countPattern: Pattern
  def extractPattern: Pattern

  def template: (ChangelogObject => String)

  def count(content: String): Int = countPattern(content, countPattern)
  
  def changelogObject(schema: String, name: String, code: String): ChangelogObject
  
  def export(content: String, outputDir: File): Unit = {
    val detected = count(content)
    if (detected == 0) {
      sys.error(s"No ${objectName} detected!")
    }

    val exported = export(content, outputDir, extractPattern, template)

    if (detected != exported) {
      sys.error(s"Detected ${detected} ${objectName}, but exported ${exported}!")
    } else {
      println(s"Exported ${exported} ${objectName}...")
    }
  }
  
  private def export(content: String, outputDir: File, pattern: Pattern,
    template: ChangelogObject => String) = {

    outputDir.mkdirs().ensuring(true)

    val matcher = pattern.matcher(content)

    var count = 0
    while (matcher.find) {
      val trimmedCode = matcher.group(0).trim
      val code = if (trimmedCode.endsWith("GO")) trimmedCode.substring(0, trimmedCode.length - 2) else trimmedCode
      val schema = matcher.group(1)
      val codeName = matcher.group(2)
      val fileName = s"${schema}.${codeName}.xml"

      val xml = template(changelogObject(schema, codeName, code))

      var writer: Option[Writer] = None
      try {
        writer = Some(new OutputStreamWriter(new FileOutputStream(new File(outputDir, fileName)), "UTF-8"))
        writer.get.write(xml)
      } finally {
        writer.map(_.close())
      }

      count += 1
    }

    count
  }

  def countPattern(content: String, pattern: Pattern) = {
    val matcher = pattern.matcher(content)
    var i = 0
    while (matcher.find) i += 1
    i
  }
}