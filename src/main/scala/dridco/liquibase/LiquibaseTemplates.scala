package dridco.liquibase

object LiquibaseTemplates {
  
  val sqlTemplate: ChangelogObject => String = (changeLogObject) => {
    s"""<?xml version="1.0" encoding="UTF-8" standalone="no"?>
    <databaseChangeLog xmlns="http://www.liquibase.org/xml/ns/dbchangelog" 
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
        xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-3.0.xsd">            
    <changeSet author="liquibase-o-matic" id="${changeLogObject.schema}.${changeLogObject.name}" runOnChange="true">
        <sql stripComments="false" splitStatements="false">
            <comment>liquibase-o-matic auto-generated drop to replace for [${changeLogObject.schema}.${changeLogObject.name}]</comment>    
            <![CDATA[
${changeLogObject.dropIfExistsTemplate(changeLogObject)}
            ]]>
        </sql>    
        <sql stripComments="false" splitStatements="false">
            <comment>liquibase-o-matic auto-generated trigger for [${changeLogObject.schema}.${changeLogObject.name}]</comment>
            <![CDATA[            
${changeLogObject.code}
            ]]>
        </sql>
    </changeSet>
</databaseChangeLog>
"""
  }
  

  val procedureTemplate: ChangelogObject => String = (changeLogObject: ChangelogObject) => { 
    s"""<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<databaseChangeLog xmlns="http://www.liquibase.org/xml/ns/dbchangelog" 
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
        xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-3.0.xsd">            
    <changeSet author="liquibase-o-matic" id="${changeLogObject.schema}.${changeLogObject.name}" runOnChange="true">
        <sql stripComments="false" splitStatements="false">
            <comment>liquibase-o-matic auto-generated drop to replace for [${changeLogObject.schema}.${changeLogObject.name}]</comment>    
            <![CDATA[
${changeLogObject.dropIfExistsTemplate(changeLogObject)}
            ]]>
        </sql>        
        <createProcedure>
            <comment>liquibase-o-matic auto-generated procedure for [${changeLogObject.schema}.${changeLogObject.name}]</comment> 
            <![CDATA[
${changeLogObject.code}
            ]]>
        </createProcedure>
    </changeSet>
</databaseChangeLog>
"""
  }
  
}
