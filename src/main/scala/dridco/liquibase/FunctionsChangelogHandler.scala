package dridco.liquibase

import java.util.regex.Pattern

class FunctionsChangelogHandler extends ChangelogHandler {
  
  def objectName = "Functions"
  def countPattern = Pattern.compile("""CREATE[\s]+FUNCTION+[\s]+""", Pattern.DOTALL | Pattern.CASE_INSENSITIVE)
  def extractPattern = Pattern.compile("""CREATE[\s]+FUNCTION+[\s]+\[([\w\d]+)\]\.\[([\w\d_]+)\](.+?)GO$""", Pattern.DOTALL | Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE | Pattern.MULTILINE)
  def template = LiquibaseTemplates.sqlTemplate
  def changelogObject(schema: String, name: String, code: String) = new FunctionChangelogObject(schema, name, code)
}