package dridco.liquibase

import java.util.regex.Pattern

class RulesChangelogHandler extends ChangelogHandler {
  def objectName = "Rules"
  def countPattern = Pattern.compile("""CREATE[\s]+RULE+[\s]+""", Pattern.DOTALL | Pattern.CASE_INSENSITIVE)
  def extractPattern = Pattern.compile("""CREATE[\s]+RULE+[\s]+\[([\w\d]+)\]\.\[([\w\d_]+)\](.+?)GO$""", Pattern.DOTALL | Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE | Pattern.MULTILINE)
  def template = LiquibaseTemplates.sqlTemplate
  def changelogObject(schema: String, name: String, code: String) = new RuleChangelogObject(schema, name, code)
}