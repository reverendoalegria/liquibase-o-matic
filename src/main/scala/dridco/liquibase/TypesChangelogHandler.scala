package dridco.liquibase

import java.util.regex.Pattern

class TypesChangelogHandler extends ChangelogHandler {

  def objectName = "Types"
  def countPattern = Pattern.compile("""CREATE[\s]+TYPE+[\s]+""", Pattern.DOTALL | Pattern.CASE_INSENSITIVE)
  def extractPattern = Pattern.compile("""CREATE[\s]+TYPE+[\s]+\[([\w\d]+)\]\.\[([\w\d_]+)\](.+?)GO[EXEC\s]+.+?GO$""", Pattern.DOTALL | Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE | Pattern.MULTILINE)
  def template = LiquibaseTemplates.sqlTemplate.andThen(_.replace("GO", "\n"))
  def changelogObject(schema: String, name: String, code: String) = new TypeChangelogObject(schema, name, code)
}