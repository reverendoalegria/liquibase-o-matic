package dridco.liquibase

import java.util.regex.Pattern

class ProceduresChangelogHandler extends ChangelogHandler {
  val objectName = "Procedures"
  val countPattern = Pattern.compile("""CREATE[\s]+[PROC|PROCEDURE]+[\s]+""", Pattern.DOTALL | Pattern.CASE_INSENSITIVE)
  def extractPattern = Pattern.compile("""CREATE[\s]+[PROC|PROCEDURE]+[\s]+\[([\w\d]+)\]\.\[([\w\d_]+)\](.+?)GO$""", Pattern.DOTALL | Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE | Pattern.MULTILINE)
  def template = LiquibaseTemplates.procedureTemplate 
  def changelogObject(schema: String, name: String, code: String) = new ProcedureChangelogObject(schema, name, code)
}